@extends('home.layout.master')
@section('breadcrump')
<div class="slide-one-item home-slider owl-carousel">
      
      <div class="site-blocks-cover overlay" style="background-image:url('{{ asset('homepage/images/wedding/2.jpg')}}');" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-left">
            <div class="col-md-3 text-left" data-aos="fade" style="position: absolute; bottom: 60px; left: 70px;">
              <!-- <h2 class="caption mb-2">Yoga for everybody</h2> -->
              <h1 class="">Only Plan with the best</h1>
              
            </div>
          </div>
          <div class="row align-items-center justify-content-right">
            <div class="col-md-5 text-right" style="position: absolute; bottom: 40px; right: 50px; color:white;">
              <p>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-whatsapp"></span></a>
                  <a href="#" class="p-2"><span class="icon-pinterest"></span></a>
                  <a href="#" class="p-2"><span class="icon-phone"></span></a>
                  <a href="#" class="p-2"><span class=""></span></a>
                  <a href="#" class="p-2"><span class="icon-comment"></span></a>


                </p>
            </div>
          </div>
        </div>
      </div>  

      <div class="site-blocks-cover overlay" style="background-image: url('{{ asset('homepage/images/wedding/3.jpg')}}');" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-left">
            <div class="col-md-3 text-left" data-aos="fade" style="position: absolute; bottom: 60px; left: 70px;">
              <!-- <h2 class="caption mb-2">Enjoy With Us</h2> -->
              <h1 class="">Only Plan with the best</h1>
            </div>
          </div>
          <div class="row align-items-center justify-content-right">
            <div class="col-md-5 text-right" style="position: absolute; bottom: 40px; right: 50px; ">
              <p style="color:white;">
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-whatsapp"></span></a>
                  <a href="#" class="p-2"><span class="icon-pinterest"></span></a>
                  <a href="#" class="p-2"><span class="icon-phone"></span></a>
                  <a href="#" class="p-2"><span class=""></span></a>
                  <a href="#" class="p-2"><span class="icon-comment"></span></a>


                </p>
            </div>
          </div>
        </div>
      </div> 
    </div>
@stop
@section('content')
      
@endsection