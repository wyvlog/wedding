@extends('home.layout.master')
@section('breadcrump')
<div class="slide-one-item home-slider owl-carousel">
      
      <div class="site-blocks-cover overlay" style="background-image:url('{{ asset('homepage/images/wedding/15.jpg')}}');" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-7 text-center" data-aos="fade">
              <!-- <h2 class="caption mb-2">Yoga for everybody</h2> -->
              <h1 class="">WEDDING GALLERY</h1>
              
            </div>
          </div>
          <div class="row align-items-center justify-content-right">
            <div class="col-md-5 text-right" style="position: absolute; bottom: 40px; right: 50px; color:white;">
              <p>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-whatsapp"></span></a>
                  <a href="#" class="p-2"><span class="icon-pinterest"></span></a>
                  <a href="#" class="p-2"><span class="icon-phone"></span></a>
                  <a href="#" class="p-2"><span class=""></span></a>
                  <a href="#" class="p-2"><span class="icon-comment"></span></a>


                </p>
            </div>
          </div>
        </div>
      </div>  

      <div class="site-blocks-cover overlay" style="background-image: url('{{ asset('homepage/images/wedding/19.jpg')}}');" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-7 text-center" data-aos="fade">
              <!-- <h2 class="caption mb-2">Enjoy With Us</h2> -->
              <h1 class="">WEDDING GALLERY</h1>
            </div>
          </div>
          <div class="row align-items-center justify-content-right">
            <div class="col-md-5 text-right" style="position: absolute; bottom: 40px; right: 50px; ">
              <p style="color:white;">
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-whatsapp"></span></a>
                  <a href="#" class="p-2"><span class="icon-pinterest"></span></a>
                  <a href="#" class="p-2"><span class="icon-phone"></span></a>
                  <a href="#" class="p-2"><span class=""></span></a>
                  <a href="#" class="p-2"><span class="icon-comment"></span></a>


                </p>
            </div>
          </div>
        </div>
      </div> 
    </div>
@stop
@section('content')
    <div class="site-section">
      <div class="">
        <div class="row">
          <div class="col-md-6 mx-auto text-center mb-5 section-heading">
            <h2 class="mb-5">Our Gallery</h2>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/1.png')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/1.png')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/2.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/2.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/3.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/3.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/4.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/4.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>

          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/5.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/5.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/6.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/6.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/7.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/7.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/8.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/8.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>

          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/9.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/9.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/10.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/10.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/11.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/11.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-6 col-lg-3">
            <a href="{{URL::asset('homepage/images/wedding/12.jpg')}}" class="image-popup img-opacity"><img src="{{URL::asset('homepage/images/wedding/12.jpg')}}" alt="Image" class="img-fluid"></a>
          </div>

        </div>
      </div>
    </div>
    
    <footer class="site-footer">
      <div class="container">
        

        <div class="row">
          <div class="col-md-4">
            <h3 class="footer-heading mb-4 text-white">About</h3>
            <p>Wedding meurupakan aplikasi yang akan membatu masyarakat luas</p>
            <p><a href="#" class="btn btn-primary pill text-white px-4">Read More</a></p>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-6">
                <h3 class="footer-heading mb-4 text-white">Quick Menu</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Destination</a></li>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Contact</a></li>
                  </ul>
              </div>
              <div class="col-md-6">
                <h3 class="footer-heading mb-4 text-white">Keunggulan</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">Mudah</a></li>
                    <li><a href="#">Cepat</a></li>
                    <li><a href="#">Berpengalaman</a></li>
                    <li><a href="#">Terjangkau</a></li>
                  </ul>
              </div>
            </div>
          </div>

          
          <div class="col-md-2">
            <div class="col-md-12"><h3 class="footer-heading mb-4 text-white">Social Media</h3></div>
              <div class="col-md-12">
                <p>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>

                </p>
              </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
      
@endsection