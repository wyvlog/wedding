@extends('home.layout.master')
@section('breadcrump')
<div class="slide-one-item home-slider owl-carousel">
      
      <div class="site-blocks-cover overlay" style="background-image:url('{{ asset('homepage/images/wedding/16.jpg')}}');" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-7 text-center" data-aos="fade">
              <!-- <h2 class="caption mb-2">Yoga for everybody</h2> -->
              <h1 class="">Contact Us</h1>
              
            </div>
          </div>
          <div class="row align-items-center justify-content-right">
            <div class="col-md-5 text-right" style="position: absolute; bottom: 40px; right: 50px; color:white;">
              <p>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-whatsapp"></span></a>
                  <a href="#" class="p-2"><span class="icon-pinterest"></span></a>
                  <a href="#" class="p-2"><span class="icon-phone"></span></a>
                  <a href="#" class="p-2"><span class=""></span></a>
                  <a href="#" class="p-2"><span class="icon-comment"></span></a>


                </p>
            </div>
          </div>
        </div>
      </div>  

      <div class="site-blocks-cover overlay" style="background-image: url('{{ asset('homepage/images/wedding/13.jpg')}}');" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-7 text-center" data-aos="fade">
              <!-- <h2 class="caption mb-2">Enjoy With Us</h2> -->
              <h1 class="">Contact Us</h1>
            </div>
          </div>
          <div class="row align-items-center justify-content-right">
            <div class="col-md-5 text-right" style="position: absolute; bottom: 40px; right: 50px; ">
              <p style="color:white;">
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-whatsapp"></span></a>
                  <a href="#" class="p-2"><span class="icon-pinterest"></span></a>
                  <a href="#" class="p-2"><span class="icon-phone"></span></a>
                  <a href="#" class="p-2"><span class=""></span></a>
                  <a href="#" class="p-2"><span class="icon-comment"></span></a>


                </p>
            </div>
          </div>
        </div>
      </div> 
    </div>
@stop
@section('content')
       

    <div class="site-section site-section-sm">
      <div class="container">
        <div class="row">
       
          <div class="col-md-12 col-lg-8 mb-5">
          
            
          
            <form action="#" class="p-5 bg-white">

              <div class="row form-group">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="fullname">Full Name</label>
                  <input type="text" id="fullname" class="form-control" placeholder="Full Name">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="email">Email</label>
                  <input type="email" id="email" class="form-control" placeholder="Email Address">
                </div>
              </div>


              <div class="row form-group">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="phone">Phone</label>
                  <input type="text" id="phone" class="form-control" placeholder="Phone #">
                </div>
              </div>

              <div class="row form-group mb-4">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="message">Message</label> 
                  <textarea name="message" id="message" cols="30" rows="5" class="form-control" placeholder="Say hello to us"></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-primary pill text-white px-5 py-2">
                </div>
              </div>

  
            </form>
          </div>

          <div class="col-lg-4">
            <div class="p-4 mb-3 bg-white">
              <h3 class="h5 text-black mb-3">Contact Info</h3>
              <p class="mb-0 font-weight-bold">Address</p>
              <p class="mb-4">Denpasar-Bali</p>

              <p class="mb-0 font-weight-bold">Phone</p>
              <p class="mb-4"><a href="#">+687 234 456 78</a></p>

              <p class="mb-0 font-weight-bold">Email Address</p>
              <p class="mb-0"><a href="#">weding@gmail.com</a></p>

            </div>
            
            
          </div>
        </div>
      </div>
    </div>
    
    <footer class="site-footer">
      <div class="container">
        

        <div class="row">
          <div class="col-md-4">
            <h3 class="footer-heading mb-4 text-white">About</h3>
            <p>Wedding meurupakan aplikasi yang akan membatu masyarakat luas</p>
            <p><a href="#" class="btn btn-primary pill text-white px-4">Read More</a></p>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-6">
                <h3 class="footer-heading mb-4 text-white">Quick Menu</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Destination</a></li>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Contact</a></li>
                  </ul>
              </div>
              <div class="col-md-6">
                <h3 class="footer-heading mb-4 text-white">Keunggulan</h3>
                  <ul class="list-unstyled">
                    <li><a href="#">Mudah</a></li>
                    <li><a href="#">Cepat</a></li>
                    <li><a href="#">Berpengalaman</a></li>
                    <li><a href="#">Terjangkau</a></li>
                  </ul>
              </div>
            </div>
          </div>

          
          <div class="col-md-2">
            <div class="col-md-12"><h3 class="footer-heading mb-4 text-white">Social Media</h3></div>
              <div class="col-md-12">
                <p>
                  <a href="#" class="pb-2 pr-2 pl-0"><span class="icon-facebook"></span></a>
                  <a href="#" class="p-2"><span class="icon-twitter"></span></a>
                  <a href="#" class="p-2"><span class="icon-instagram"></span></a>
                  <a href="#" class="p-2"><span class="icon-vimeo"></span></a>

                </p>
              </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
      
@endsection