    <div class="site-navbar-wrap js-site-navbar bg-white">
      
      <div class="container">
        <div class="site-navbar bg-light">
          <div class="py-1">
            <div class="row align-items-center" style="flex-wrap: nowrap">
              <div class="col-2">
                <a href="#"><img style="width:100%; margin-left:-200px;"src="{{URL::asset('homepage/images/Logo.png')}}" alt="Image" class="img-fluid"></a>
              </div>
              <div class="col-12">

                <nav class="site-navigation text-right" role="navigation">
                  <div class="container">
                    
                    <div class="d-inline-block d-lg-none  ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu h3"></span></a></div>
                    <ul class="site-menu js-clone-nav d-none d-lg-block">
                        
                      <li class="active" style="display: inline-block;">
                        <a href="{{route('/')}}">HOME</a>
                      </li>
                      <li class="has-children">
                        <a href="{{route('/')}}">DESTINATION</a>
                        <ul class="dropdown arrow-top">
                          <li class="has-children">
                            <a href="classes.html">BALI</a>
                            <ul class="dropdown">
                              <li><a href="{{route('/')}}">UBUD</a></li>
                              <li><a href="{{route('/')}}">JIMBARAN</a></li>
                              <li><a href="{{route('/')}}">ULUWATU</a></li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                      <li >
                        <a href="{{route('gallery')}}">GALLERY</a>
                      </li>
                      <li >
                        <a href="{{route('/')}}">ABOUT</a>
                      </li>
                      <li >
                        <a href="{{route('/')}}">PREFERED COMPANIS</a>
                      </li>
                      <li>
                        <a href="{{route('/')}}">TESTIMONY</a>
                      </li>
                      <li><a href="{{route('blog')}}">BLOG</a></li>
                      <li><a href="{{route('/')}}">FAQ</a></li>
                      <li><a href="{{route('contact')}}">CONTACT</a></li>
                      <li style="display: inline-block;"><a style="background-color: #CECECE; border-radius: 20px; padding: 5px 20px;" href="{{route('/')}}">LOG IN</a></li>
                    </ul>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>