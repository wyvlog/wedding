<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Weding</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700|Work+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('homepage/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{URL::asset('homepage/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('homepage/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{URL::asset('homepage/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{URL::asset('homepage/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('homepage/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('homepage/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{URL::asset('homepage/css/animate.css')}}">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    
    <link rel="stylesheet" href="{{URL::asset('homepage/fonts/flaticon/font/flaticon.css')}}">
  
    <link rel="stylesheet" href="{{URL::asset('homepage/css/aos.css')}}">

    <link rel="stylesheet" href="{{URL::asset('homepage/css/style.css')}}">
    
  </head>
  <body>
    <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->

    @include ('home.include.header')

    @yield('breadcrump')
    
    @yield('content')


@yield('script')

<script src="{{URL::asset('homepage/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/jquery-ui.js')}}"></script>
  <script src="{{URL::asset('homepage/js/popper.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/bootstrap.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/owl.carousel.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/jquery.stellar.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/jquery.countdown.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{URL::asset('homepage/js/aos.js')}}"></script>

  
  <script src="{{URL::asset('homepage/js/mediaelement-and-player.min.js')}}"></script>

  <script src="{{URL::asset('homepage/js/main.js')}}"></script>
    

  <script>
      document.addEventListener('DOMContentLoaded', function() {
                var mediaElements = document.querySelectorAll('video, audio'), total = mediaElements.length;

                for (var i = 0; i < total; i++) {
                    new MediaElementPlayer(mediaElements[i], {
                        pluginPath: 'https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/',
                        shimScriptAccess: 'always',
                        success: function () {
                            var target = document.body.querySelectorAll('.player'), targetTotal = target.length;
                            for (var j = 0; j < targetTotal; j++) {
                                target[j].style.visibility = 'visible';
                            }
                  }
                });
                }
            });
    </script>
</body>
</html>
